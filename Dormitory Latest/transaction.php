<?php include 'includes/session.php'; ?>
<?php
if (!isset($_SESSION['student']) || trim($_SESSION['student']) == '') {
	header('index.php');
}

$stuid = $_SESSION['student'];
$sql = "SELECT * FROM borrow LEFT JOIN equipments ON equipments.id=borrow.equipment_id WHERE student_id = '$stuid' ORDER BY date_borrow DESC";
$action = '';
if (isset($_GET['action'])) {
	$sql = "SELECT * FROM returns LEFT JOIN equipments ON equipments.id=returns.equipment_id WHERE student_id = '$stuid' ORDER BY date_return DESC";
	$action = $_GET['action'];

}

?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">

		<?php include 'includes/navbar.php'; ?>

		<div class="content-wrapper bg-gradient-default">
			<div class="container">

				<!-- Main content -->
				<section class="content">
					<?php
					if (isset($_SESSION['error'])) {
					?>
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-warning"></i> Error!</h4>
							<ul>
								<?php
								foreach ($_SESSION['error'] as $error) {
									echo "
                      <li>" . $error . "</li>
                    ";
								}
								?>
							</ul>
						</div>
					<?php
						unset($_SESSION['error']);
					}

					if (isset($_SESSION['success'])) {
						echo "
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4><i class='icon fa fa-check'></i> Success!</h4>
              " . $_SESSION['success'] . "
            </div>
          ";
						unset($_SESSION['success']);
					}
					?>
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="box">
								<div class="box-header with-border">
									<a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-rounded"><i class="fa fa-plus"></i> Borrow</a>
								</div>
								<div class="box-header with-border">
									<h3 class="box-title">Borrowed & Returned Transaction</h3>
									<div class="pull-right">
										<select class="form-control input-sm" id="transelect">
											<option value="borrow" <?php echo ($action == '') ? 'selected' : ''; ?>>Borrow</option>
											<option value="return" <?php echo ($action == 'return') ? 'selected' : ''; ?>>Return</option>
										</select>
									</div>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped" id="example1">
										<thead>
											<th class="hidden"></th>
											<th>Date</th>
											<th>Equipment Code</th>
											<th>Equipment Name</th>

										</thead>
										<tbody>
											<?php

											$query = $conn->query($sql);
											while ($row = $query->fetch_assoc()) {

												 
												$date = (isset($_GET['action'])) ? 'date_return' : 'date_borrow';
												echo "
			        						<tr>
			        							<td class='hidden'></td>
			        							<td>" . date('M d, Y', strtotime($row[$date])) . "</td>
			        							<td>" . $row['code'] . "</td>
			        							<td>" . $row['title'] . "</td>


			        						</tr>
			        					";
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>

		<?php include 'includes/footer.php'; ?>
		<?php include 'includes/borrow_modal.php'; ?>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script>
		$('#transelect').on('change', function() {
			var action = $(this).val();
			if (action == 'borrow') {
				window.location = 'transaction.php';
			} else {
				window.location = 'transaction.php?action=' + action;
			}
		});
	</script>
</body>

</html>