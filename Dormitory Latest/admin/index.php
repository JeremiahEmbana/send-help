<?php
  	session_start();
  	if(isset($_SESSION['admin'])){
    	header('location:home.php');
  	}
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Login | LNU Dormitory</title>
    <link rel="stylesheet" href="assets/css/main.css" type="text/css">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/css/all.min.css" />
  <link rel="icon" href="assets/img/logo.png" type="image/png">
  <link rel="stylesheet" href="assets/css/logo.css" type="text/css">
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="assets/js/all.min.js"></script>

</head>
<body class="bg-default hold-transition login-page">
<!-- Header -->
<div class="header bg-gradient-warning py-1">
    <div class="container">
      <div class="header-body text-center mb-6">
        <div class="row justify-content-center">
          <div class="col-xl-5 col-lg-6 col-md-8 px-5 mt-6">
            <h1 class="text-white">Leyte Normal University</h1>
            <p class="text-lead text-white">Student Dormitory</p>
          </div>
        </div>
      </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
      <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
      </svg>
    </div>
  </div>

  <!-- Main content -->
  <div class="main-content">
   
    <!-- Page content -->
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          
          <div class="card bg-secondary border-0 mb-0">

            <div class="card-body px-lg-5">
            <img src="assets/img/logo.png" class="centerL">
        

              <div class="text-center text-muted mb-4">
                <big>Admin Login</big>
              </div>
			  <?php
  		if(isset($_SESSION['error'])){
  			echo "
  				<div class='callout callout-danger text-center mt20'>
			  		<p>".$_SESSION['error']."</p> 
			  	</div>
  			";
  			unset($_SESSION['error']);
  		}
  	?>
    	<form action="login.php" method="POST">
		<div class="form-group mb-3">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-user fa-lg fa-fw"></i></span>
                    </div>
                    <input class="form-control" name = "username" placeholder="Enter Username" type = "text" required autofocus>
                  </div>
                </div>
				<div class="form-group">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-key fa-lg fa-fw"></i></span>
                    </div>
                    <input class="form-control" name = "password" placeholder="Enter Password" type="password" required>
                  </div>
                </div>
    			<div class="form-group">
					  <input type="submit" class="btn btn-warning btn-block btn-md rounded-3 mt-5" name="login" value="Sign In">
      		</div>
    	</form>
  	</div>

	  </div>
          </div>    
        </div>
      </div>
  
</div>
	
<?php include 'includes/scripts.php' ?>

<!-- Footer -->
<footer class="py-5" id="footer-main">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
            &copy; 2021 <a href="https://www.lnu.edu.ph/" class="font-weight-bold ml-1" target="_blank">Leyte Normal University</a>
          </div>
        </div>
        
      </div>
    </div>
  </footer>

  <!-- jQuery CDN -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="assets/js/all.min.js"></script>
</body>
</html>

