<?php
session_start();

if(!isset($_SESSION['username'])){
    header('location:index.php');
    exit();

    
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <?php
    $title = basename($_SERVER['PHP_SELF'], '.php');
    $title = explode('-', $title);
    $title = ucfirst($title[1]);
    ?>
    <title><?= $title; ?> | Admin Panel</title>
    <link rel="icon" href="assets/img/logo.png" type="image/png">
    <!-- Bootstrap 4 CSS CDN -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <!-- Fontawesome CSS CDN -->
    <link rel="stylesheet" href="assets/css/all.min.css" />

    <link rel="stylesheet" type="text/css" href="assets/css/datatables.min.css"/>

    <!-- jQuery CDN -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="assets/js/all.min.js" defer></script>

<script type="text/javascript">
    $(document).ready(function(){
      $("#open-nav").click(function(){
        $(".admin-nav").toggleClass('animate');
        });
    });
</script>

<style type="text/css">
.bg-gradient-default
{
    background: linear-gradient(87deg, #172b4d 0, #1a174d 100%) !important;
}
.bg-gradient-warning
{
    background: linear-gradient(87deg, #fb6340 0, #fbb140 100%) !important;
}
.bg-default
{
    background-color: #172b4d !important;
}
.bg-gradient-dark
{
    background: linear-gradient(87deg, #3f4041 0, #727485 100%) !important;
}
.bg-gradient-info
{
    background: linear-gradient(87deg, #11cdef 0, #1171ef 100%) !important;
}
.bg-gradient-danger
{
    background: linear-gradient(87deg, #f5365c 0, #f56036 100%) !important;
}
.bg-gradient-blue
{
    background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%) !important;
}
.bg-gradient-green
{
    background: linear-gradient(87deg, #2dce89 0, #2dcecc 100%) !important;
}

.bg-gradient-teal
{
    background: linear-gradient(87deg, #11cdef 0, #1171ef 100%) !important;
}
.bg-gradient-pink
{
    background: linear-gradient(87deg, #f3a4b5 0, #f3b4a4 100%) !important;
}
.bg-gradient-gray
{
    background: linear-gradient(87deg, #8898aa 0, #888aaa 100%) !important;
}

.bg-gradient-gray-dark
{
    background: linear-gradient(87deg, #32325d 0, #44325d 100%) !important;
}
.bg-gradient-indigo
{
    background: linear-gradient(87deg, #5603ad 0, #9d03ad 100%) !important;
}

.col-auto
{
    width: auto;
    max-width: 100%; 

    flex: 0 0 auto;
}


.admin-nav{
    width: 220px;
    min-height: 100vh;
    overflow: hidden;
    transition: 0.3s all ease-in-out;
    
}
.admin-link{
    background-color: transparent;
}
.admin-link:hover, .nav-active{
    background-color: #1a174d;
    text-decoration: none;
}
.animate{
    width: 0;
    transition: 0.3s all ease-in-out;
}
</style>
</head>

<body class="bg-gradient-default">
    
    <div class="container-fluid">
      <div class="row">
        <div class="admin-nav bg-gradient-warning p-0">
          <h4 class="text-light text-center p-2">LNU | DORM</h4>
        <div class="list-group list-group-flush">

          <a href="admin-dashboard.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-dashboard.php') ?"nav-active":""; ?>">
            <i class="fas fa-chart-pie fa-lg mr-3"></i>&nbsp;Dashboard
          </a>
          <a href="admin-users.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-users.php') ?"nav-active":""; ?>">
            <i class="fas fa-user-friends fa-lg mr-3"></i>Users/Students
          </a>
          <a href="admin-notes.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-notes.php') ?"nav-active":""; ?>">
            <i class="fas fa-sticky-note fa-lg mr-4"></i>Logs
          </a>
          <a href="admin-notification.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-notification.php') ?"nav-active":""; ?>">
            <i class="fas fa-bell fa-lg mr-4"></i>Notifications&nbsp;&nbsp;&nbsp;&nbsp;<span id="checkNotification"></span>
          </a>
          <a href="admin-feedback.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-feedback.php') ?"nav-active":""; ?>">
            <i class="fas fa-comment fa-lg mr-3"></i>&nbsp;Feedbacks
          </a>
          <a href="admin-deleteduser.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-deleteduser.php') ?"nav-active":""; ?>">
            <i class="fas fa-user-slash fa-lg mr-1"></i>&nbsp;&nbsp;Deleted Users
          </a>

          <a href="assets/php/admin-action.php?export=excel" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == '#') ?"nav-active":""; ?>">
            <i class="fas fa-table fa-lg mr-2"></i>&nbsp;&nbsp;Export Users
          </a>

          <a href="assets/php/admin-action.php?export2=excel" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == '#') ?"nav-active":""; ?>">
            <i class="fas fa-table fa-lg mr-2"></i>&nbsp;&nbsp;Generate &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Attendance Sheet
          </a>

          <a href="admin-profile.php" class="list-group-item text-light admin-link <?= (basename($_SERVER['PHP_SELF']) == 'admin-profile.php') ?"nav-active":""; ?>">
            <i class="fas fa-id-card fa-lg mr-3"></i>Profile
          </a>

        </div>
        </div>

        <div class="col">
            <div class="row">
                <div class="col-lg-12 bg-default pt-2 justify-content-between d-flex">
                    <a href="#" class="text-white" id="open-nav">
                        <h3><i class="fas fa-bars"></i></h3>
                    </a>
            <h4 class="text-light"><?= $title; ?></h4>
            <a href="assets/php/logout.php" class="text-light mt-1"><i class="fas fa-sign-out-alt"></i>&nbsp;Logout</a>
                </div>
            </div>
