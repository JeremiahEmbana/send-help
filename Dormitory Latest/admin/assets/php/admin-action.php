<?php
require_once 'admin-db.php';
$admin = new Admin();
session_start();

//Handle Admin Login Ajax Req
if(isset($_POST['action']) && $_POST['action'] == 'adminLogin'){
    $username = $admin->test_input($_POST['username']);
    $password = $admin->test_input($_POST['password']);

    $hpassword = sha1($password);

    $loggedInAdmin = $admin->admin_login($username,$hpassword);

    if($loggedInAdmin != null){
        echo 'admin_login';
        $_SESSION['username'] = $username;
    }
    else{
        echo $admin->showMessage('danger', 'Username or Password is Incorrect');

    }
}


//handle fetch all users ajax req
if(isset($_POST['action']) && $_POST['action'] == 'fetchAllUsers'){
    $output = '';
    $data = $admin->fetchAllUsers(0);
    $path = '../assets/php/';

    if($data){
        $output .= '<table class="table table-striped table-bordered text-center">
                    <thead>
                    <tr>
                    <th>Student No.</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone No.</th>
                    <th>Gender</th>
                    <th>Room No.</th>
                    <th>Verified(1) | Unverified(0)</th>
                    <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $row) {
            if($row['photo'] != ''){
                $uphoto = $path.$row['photo'];
            }
            else{
                $uphoto = '../assets/img/logo.png';
            }
            $output .= '<tr>
            <td>'.$row['student_id'].'</td>
            <td><img src="'.$uphoto.'" class="rounded-circle" width="40px"></td>
            <td>'.$row['name'].'</td>
            <td>'.$row['email'].'</td>
            <td>'.$row['address'].'</td>
            <td>'.$row['phone'].'</td>
            <td>'.$row['gender'].'</td>
            <td>'.$row['room'].'</td>
            <td>'.$row['verified'].'</td>
            <td>
              <a href="#" student_id="'.$row['student_id'].'" title="View Details"
               class="text-info userDetailsIcon" data-toggle="modal" data-target="#showUserDetailsModal"><i class="fas fa-info-circle fa-lg"></i></a>
               &nbsp;&nbsp;

               <a href="#" student_id="'.$row['student_id'].'" title="Delete User"
               class="text-danger deleteUserIcon"><i class="fas fa-trash-alt fa-lg"></i></a>
               &nbsp;&nbsp;

            </td>
            </tr>';
        }
        $output .='</tbody>
                </table>';
        echo $output;
        }
        else{
            echo '<h3 class="text-center text-secondary">:( No currently Reigistered Users</h3>';
        }               
    }

    //handle display user in details ajax req
    if(isset($_POST['details_id'])){
        $student_id = $_POST['details_id'];

        $data = $admin->fetchUserDetailsByID($student_id);

        echo json_encode($data);
    }

    //Handle Delete user ajax req
    if(isset($_POST['del_id'])){
        $student_id = $_POST['del_id'];
        $admin->userAction($student_id, 0);
    }

    //Handle fetch all deleted users ajax req
    if(isset($_POST['action']) && $_POST['action'] == 'fetchAllDeletedUsers'){
        $output = '';
        $data = $admin->fetchAllUsers(1);
        $path = '../assets/php/';
    
        if($data){
            $output .= '<table class="table table-striped table-bordered text-center">
                        <thead>
                        <tr>
                        <th>Student No.</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone No.</th>
                        <th>Gender</th>
                        <th>Room No.</th>
                        <th>Verified(1) | Unverified(0)</th>
                        <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>';
            foreach ($data as $row) {
                if($row['photo'] != ''){
                    $uphoto = $path.$row['photo'];
                }
                else{
                    $uphoto = '../assets/img/logo.png';
                }
                $output .= '<tr>
                <td>'.$row['student_id'].'</td>
                <td><img src="'.$uphoto.'" class="rounded-circle" width="40px"></td>
                <td>'.$row['name'].'</td>
                <td>'.$row['email'].'</td>
                <td>'.$row['address'].'</td>
                <td>'.$row['phone'].'</td>
                <td>'.$row['gender'].'</td>
                <td>'.$row['room'].'</td>
                <td>'.$row['verified'].'</td>
                <td>
                   <a href="#" student_id="'.$row['student_id'].'" title="Restore User"
                   class="text-white restoreUserIcon badge badge-dark p-2">Restore</a>
    
                </td>
                </tr>';
            }
            $output .='</tbody>
                    </table>';
            echo $output;
            }
            else{
                echo '<h3 class="text-center text-secondary">:( No currently Deleted Users</h3>';
            }               
        }

    //Handel Restore Deleted user ajax req
    if(isset($_POST['res_id'])){
        $student_id = $_POST['res_id'];

        $admin->userAction($student_id, 1);
    }

    //Handle fetch all notes ajax req
    if(isset($_POST['action']) && $_POST['action'] == 'fetchAllNotes'){
        $output = '';
        $note = $admin->fetchAllNotes();
    
        if($note){
            $output .= '<table class="table table-striped table-bordered text-center">
                        <thead>
                        <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>User E-mail</th>
                        <th>Note Title</th>
                        <th>Note</th>
                        <th>Written On</th>
                        <th>Updated On</th>
                        <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>';
            foreach ($note as $row) {
               
                $output .= '<tr>
                <td>'.$row['id'].'</td>
                <td>'.$row['name'].'</td>
                <td>'.$row['email'].'</td>
                <td>'.$row['title'].'</td>
                <td>'.$row['note'].'</td>
                <td>'.$row['created_at'].'</td>
                <td>'.$row['updated_at'].'</td>
                <td>
                   <a href="#" id="'.$row['id'].'" title="Delete Note"
                   class="text-danger deleteNoteIcon"><i class="fas fa-trash-alt fa-lg"></i></a>
    
                </td>
                </tr>';
            }
            $output .='</tbody>
                    </table>';
            echo $output;
            }
            else{
                echo '<h3 class="text-center text-secondary">:( No currently written Notes</h3>';
            }               
        }

    //handle delete note of user ajax req
    if(isset($_POST['note_id'])){
        $id = $_POST['note_id'];

        $admin->deleteNoteOfUser($id);
    }

   

//handle change pass  ajax req
if(isset($_POST['action']) && $_POST['action'] == 'change_pass'){
    $currentPass = $_POST['curpass'];
    $newPass = $_POST['newpass'];
    $cnewPass = $_POST['cnewpass'];

    $hnewPass = password_hash($newPass, PASSWORD_DEFAULT);

    if($newPass != $cnewPass){
        echo $cuser->showMessage('danger','Password did not matched!');
    }
    else{
        if(password_verify($currentPass, $cpass)){
            $cuser->change_password($hnewPass,$cid);
            echo $cuser->showMessage('success','Password Changed Successfully!');
            $cuser->notification($cid, 'admin', 'Password Changed');
        }
        else{
            echo $cuser->showMessage('danger','Current Password is Incorrect!');
        }
    }
}

//Handle fetch all feedback ajax req
if(isset($_POST['action']) && $_POST['action'] == 'fetchAllFeedback'){
    $output = '';
    $feedback = $admin->fetchFeedback();

    if($feedback){
        $output .= '<table class="table table-striped table-bordered text-center">
                    <thead>
                    <tr>
                    <th>#</th>
                    <th>Student ID</th>
                    <th>User Name</th>
                    <th>User E-mail</th>
                    <th>Subject</th>
                    <th>Feedback</th>
                    <th>Sent On</th>
                    <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($feedback as $row) {
           
            $output .= '<tr>
            <td>'.$row['id'].'</td>
            <td>'.$row['uid'].'</td>
            <td>'.$row['name'].'</td>
            <td>'.$row['email'].'</td>
            <td>'.$row['subject'].'</td>
            <td>'.$row['feedback'].'</td>
            <td>'.$row['created_at'].'</td>
            <td>
               <a href="#" fid="'.$row['id'].'" id="'.$row['uid'].'" title="Reply"
               class="text-info replyFeedbackIcon" data-toggle="modal" data-target="#showReplyModal"><i class="fas fa-reply fa-lg"></i></a>

            </td>
            </tr>';
        }
        $output .='</tbody>
                </table>';
        echo $output;
        }
        else{
            echo '<h3 class="text-center text-secondary">:( No currently written Feedbacks</h3>';
        }               
    }

    //Handle Reply feedback to user ajax req
    if(isset($_POST['message'])){
        $uid = $_POST['uid'];
        $message = $admin->test_input($_POST['message']);
        $fid = $_POST['fid'];

        $admin->replyFeedback($uid, $message);
        $admin->feedbackReplied($fid);
    }

    //Handle fetch Notification ajax req
    if(isset($_POST['action']) && $_POST['action'] == 'fetchNotification'){
        $notification = $admin->fetchNotification();
        $output = '';
    
        if($notification){
            foreach ($notification as $row) {
                $output .= '<div class="alert alert-warning" role="alert">
                <button type="button" id="'.$row['id'].'" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="alert-heading">New Notification</h4>
                <p class="mb-0 lead">'.$row['message'].' by '.$row['name'].'</p>
                <hr class="my-2">
                <p class="mb-0 float-left"><b>User Email :'.$row['email'].'</b></p>
                <p class="mb-0 float-right">'.$admin->timeInAgo($row['created_at']).'</p>
                <div class="clearfix"></div>
              </div>';
            }
            echo $output;
        }
        else{
            echo '<h3 class="text-center text-secondary mt-5">No current Notification</h3>';
        }
    }

    //Handle Check Notification
    if(isset($_POST['action']) && $_POST['action'] == 'checkNotification'){
        if($admin->fetchNotification()){
            echo '<i class="fas fa-envelope-open-text fa-lg text-success"></i>';
        }
        else{
            echo '<i class="fas fa-envelope-open fa-lg text-danger"></i>';
        }
    }

    //Handle Remove Notification
    if(isset($_POST['notification_id'])){
        $id = $_POST['notification_id'];
        $admin->removeNotification($id);
    }

    //Handle export all users in excel
    if(isset($_GET['export']) && $_GET['export'] == 'excel'){
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=users.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    
    $data = $admin->exportAllUsers();
    echo '<table border="1" align=center>';

    echo '<tr>
            <th>Student No.</th>
            <th>Name</th>
            <th>Email Address</th>
            <th>Room No.</th>
            <th>Address</th>
            <th>Phone No.</th>
            <th>Gender</th>
            <th>Date of Birth</th>
            <th>Joined ON</th>
            <th>Verification Status</th>
            <th>Account Status</th>
          </tr>';
    foreach ($data as $row) {
        echo '<tr>
                <td>'.$row['student_id'].'</td>
                <td>'.$row['name'].'</td>
                <td>'.$row['email'].'</td>
                <td>'.$row['room'].'</td>
                <td>'.$row['address'].'</td>
                <td>'.$row['phone'].'</td>
                <td>'.$row['gender'].'</td>
                <td>'.$row['dob'].'</td>
                <td>'.$row['created_at'].'</td>
                <td>'.$row['verified'].'</td>
                <td>'.$row['deleted'].'</td>
              </tr>';
    }
   echo '</table>';
    }

    //Handle export all users in excel
    if(isset($_GET['export2']) && $_GET['export2'] == 'excel'){
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=attendanceSheet.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        
        $data = $admin->exportAllUsers();
        echo '<table border="1" align=center>';
    
        echo '<tr>
                <th>Student No.</th>
                <th>Name</th>
                <th>Address</th>
                <th>Phone No.</th>
                <th>Gender</th>
                <th>Room No.</th>
                <th>Signature</th>
              </tr>';
        foreach ($data as $row) {
            echo '<tr>
                    <td>'.$row['student_id'].'</td>
                    <td>'.$row['name'].'</td>
                    <td>'.$row['address'].'</td>
                    <td>'.$row['phone'].'</td>
                    <td>'.$row['gender'].'</td>
                    <td>'.$row['room'].'</td>
                    <td></td>
                  </tr>';
        }
       echo '</table>';
        }
?>