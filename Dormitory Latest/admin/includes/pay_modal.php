<div class="modal fade" id="pay">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b>Confirm Payment?</b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="pay_add.php">
              <div class="form-group">
                  	<label for="student_id" class="col-sm-3 control-label">Student ID</label>

                  	<div class="col-sm-9">
                    	<input type="text" class="form-control" id="student_id" name="student_id" required>
                  	</div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-rounded pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-rounded" name="pay"><i class="fa fa-check"></i> Confirm</button>
              </form>
            </div>
        </div>
    </div>
</div>