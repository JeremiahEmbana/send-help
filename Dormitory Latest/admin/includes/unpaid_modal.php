<!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
          	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
              		<span aria-hidden="true">&times;</span></button>
            	<h4 class="modal-title"><b>Record Unpaid Students</b></h4>
          	</div>
          	<div class="modal-body">
            	<form class="form-horizontal" method="POST" action="unpaid_add.php">
          		  <div class="form-group">
                  	<label for="student_id" class="col-sm-3 control-label">Student ID</label>

                  	<div class="col-sm-9">
                    	<input type="text" class="form-control" id="student_id" name="student_id" required>
                  	</div>	
                </div>

                <div class="form-group">
                  	<label for="date_from" class="col-sm-3 control-label">Date From</label>

                  	<div class="col-sm-9">
                    	<input type="date" class="form-control" id="date_from" name="date_from" required>
                  	</div>
                </div>
                <div class="form-group">
                  	<label for="date_to" class="col-sm-3 control-label">Date To</label>

                  	<div class="col-sm-9">
                    	<input type="date" class="form-control" id="date_to" name="date_to" required>
                  	</div>
                </div>
				

                <!-- <span id="append-div"></span>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                      <button class="btn btn-primary btn-xs btn-flat" id="append"><i class="fa fa-plus"></i> Equipment Field</button>
                    </div>
                </div> -->
          	</div>
          	<div class="modal-footer">
            	<button type="button" class="btn btn-default btn-rounded pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            	<button type="submit" class="btn btn-primary btn-rounded" name="add"><i class="fa fa-save"></i> Save</button>
            	</form>
          	</div>
        </div>
    </div>
</div>
