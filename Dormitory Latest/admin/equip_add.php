<?php
	include 'includes/session.php';

	if(isset($_POST['add'])){
		$code = $_POST['code'];
		$title = $_POST['title'];
		$category = $_POST['category'];
		$quantity = $_POST['quantity'];

		//creating code
		$letters = '';
		$numbers = '';
		foreach (range('A', 'Z') as $char) {
		    $letters .= $char;
		}
		for($i = 0; $i < 10; $i++){
			$numbers .= $i;
		}
		$code = substr(str_shuffle($letters), 0, 1).substr(str_shuffle($numbers), 0, 2);
		//

		$sql = "INSERT INTO equipments (code, category_id, title, quantity) VALUES ('$code', '$category', '$title', '$quantity')";
		if($conn->query($sql)){
			$_SESSION['success'] = 'Equipment added successfully';
		}
		else{
			$_SESSION['error'] = $conn->error;
		}
	}	
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}

	header('location: equipment.php');

?>