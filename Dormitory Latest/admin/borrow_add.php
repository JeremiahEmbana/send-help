<?php
include 'includes/session.php';

if (isset($_POST['add'])) {
	$student = $_POST['student'];

	$sql = "SELECT * FROM students WHERE student_id = '$student'";
	$query = $conn->query($sql);
	if ($query->num_rows < 1) {
		if (!isset($_SESSION['error'])) {
			$_SESSION['error'] = array();
		}
		$_SESSION['error'][] = 'Student not found';
	} else {
		$row = $query->fetch_assoc();
		$student_id = $row['student_id'];

		$added = 0;
		foreach ($_POST['code'] as $code) {
			if (!empty($code)) {
				$sql = "SELECT * FROM equipments WHERE code = '$code' AND status != 1";
				$query = $conn->query($sql);
				if ($query->num_rows > 0) {
					$brow = $query->fetch_assoc();
					$quantity = $brow['quantity'];
					$bid = $brow['id'];

					$sql = "INSERT INTO borrow (student_id, equipment_id, date_borrow) VALUES ('$student_id', '$bid', NOW())";
					if ($conn->query($sql)) {
						$added++;
						$sql = "UPDATE equipments SET quantity = $quantity - 1, status = 0 WHERE id = '$bid'";
						$conn->query($sql);

						$sql = "SELECT * FROM equipments WHERE code = '$code'";
						$query = $conn->query($sql);

						if ($query->num_rows > 0) {
							$quantity = $brow['quantity'];

							if ($quantity == 0) {
								$sql = "UPDATE equipments SET  status = 1 WHERE id = '$bid'";
								$conn->query($sql);
							}
						}
					} else {
						if (!isset($_SESSION['error'])) {
							$_SESSION['error'] = array();
						}
						$_SESSION['error'][] = $conn->error;
					}
				} else {
					if (!isset($_SESSION['error'])) {
						$_SESSION['error'] = array();
					}
					$_SESSION['error'][] = 'Equipment with code - ' . $code . ' unavailable';
				}
			}
		}

		if ($added > 0) {
			$equipments = ($added == 1) ? 'Equipment' : 'Equipments';
			$_SESSION['success'] = $added . ' ' . $equipments . ' successfully borrowed';
		}
	}
} else {
	$_SESSION['error'] = 'Fill up add form first';
}

header('location: borrow.php');
