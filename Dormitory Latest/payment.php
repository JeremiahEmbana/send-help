<?php include 'includes/session.php'; ?>
<?php
if (!isset($_SESSION['student']) || trim($_SESSION['student']) == '') {
	header('index.php');
}

$stuid = $_SESSION['student'];
$sql = "SELECT * FROM paid WHERE student_id = '$stuid' ORDER BY date_paid DESC";
$action = '';
if (isset($_GET['action'])) {
	$sql = "SELECT * FROM unpaid WHERE student_id = '$stuid' ORDER BY date_unpaid DESC";
	$action = $_GET['action'];

}

?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue layout-top-nav">
	<div class="wrapper">

		<?php include 'includes/navbar.php'; ?>

		<div class="content-wrapper bg-gradient-default">
			<div class="container">

				<!-- Main content -->
				<section class="content">
					
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="box">

								<div class="box-header with-border">
									<h3 class="box-title">Payment Record</h3>
									<div class="pull-right">
										<select class="form-control input-sm" id="transelect">
											<option value="paid" <?php echo ($action == '') ? 'selected' : ''; ?>>Paid</option>
											<option value="unpaid" <?php echo ($action == 'unpaid') ? 'selected' : ''; ?>>Unpaid</option>
										</select>
									</div>
								</div>
								<div class="box-body">
									<table class="table table-bordered table-striped" id="example1">
										<thead>
											<th class="hidden"></th>

											<th>Valid From</th>
											<th>Valid To</th>
											<th>Status</th>
										</thead>
										<tbody>
											<?php
											
											$query = $conn->query($sql);
											while ($row = $query->fetch_assoc()) {
												if ($row['status']) {
													$status = '<span class="label label-success">Paid</span>';
												  } else {
													$status = '<span class="label label-danger">Unpaid</span>';
												  }
												$date = (isset($_GET['action'])) ? 'date_paid' : 'date_unpaid';
												echo "
			        						<tr>
			        							<td class='hidden'></td>

			        							<td>" . date('M d, Y', strtotime($row['date_from'])) . "</td>
			        							<td>" . date('M d, Y', strtotime($row['date_to'])) . "</td>
												<td>" . $status . "</td>
			        						</tr>
			        					";
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</div>

		<?php include 'includes/footer.php'; ?>
	</div>

	<?php include 'includes/scripts.php'; ?>
	<script>
		$('#transelect').on('change', function() {
			var action = $(this).val();
			if (action == 'paid') {
				window.location = 'payment.php';
			} else {
				window.location = 'payment.php?action=' + action;
			}
		});
	</script>
</body>

</html>