<header class="main-header">
  <nav class="navbar navbar-static-top bg-gradient-warning">
    <div class="container">
      <div class="navbar-header">
        <a href="#" class="navbar-brand"><b>LNU</b>|Dormitory</a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <?php
            if(isset($_SESSION['student'])){
              echo "
                <li><a href='index.php'><i class='fa fa-book'></i><strong>Equipments</strong></a></li>
                <li><a href='transaction.php'><i class='fa fa-refresh'></i><strong>Borrowed&Returned</strong></a></li>
                <li><a href='payment.php'><i class='fa fa-table'></i><strong>Payment Records</strong></a></li>
              ";
            } 
          ?>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php
            if(isset($_SESSION['student'])){
              $photo = (!empty($student['photo'])) ? 'images/'.$student['photo'] : 'images/profile.jpg';
              echo "
                <li class='user user-menu'>
                  <a href='#profile' data-toggle='modal' id='student_profile'>
                    <img src='".$photo."' class='user-image' alt='User Image'>
                    <strong><span class='hidden-xs'>".$student['firstname'].' '.$student['lastname']."</span></strong>
                  </a>
                </li>
                <li><a href='logout.php'><i class='fa fa-sign-out'></i> <strong>LOGOUT</strong></a></li>
              ";
            }
            else{
              echo "
                <li><a href='#login' data-toggle='modal'><i class='fa fa-sign-in'></i> <strong>LOGIN</strong></a></li>
              ";
            } 
          ?>
        </ul>
      </div>
      <!-- /.navbar-custom-menu -->
    </div>
    <!-- /.container-fluid -->
  </nav>
</header>

<?php include 'includes/login_modal.php'; ?>
<?php include 'includes/profile_modal.php'; ?>