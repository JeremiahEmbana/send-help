<?php
include 'includes/session.php';


if (isset($_POST['add'])) {

	$row = $query->fetch_assoc();
	$student_id = $_SESSION['student'];

	$added = 0;
	foreach ($_POST['code'] as $code) {
		if (!empty($code)) {
			$sql = "SELECT * FROM equipments WHERE code = '$code' AND status != 1";
			$query = $conn->query($sql);
			if ($query->num_rows > 0) {
				$brow = $query->fetch_assoc();
				$quantity = $brow['quantity'];
				$bid = $brow['id'];

				$sql = "INSERT INTO borrow (student_id, equipment_id, date_borrow) VALUES ('$student_id', '$bid', NOW())";
				if ($conn->query($sql)) {
					$added++;
					$sql = "UPDATE equipments SET quantity = $quantity - 1, status = 0 WHERE id = '$bid'";
					$conn->query($sql);
				} else {
					if (!isset($_SESSION['error'])) {
						$_SESSION['error'] = array();
					}
					$_SESSION['error'][] = $conn->error;
				}
			} else {
				if (!isset($_SESSION['error'])) {
					$_SESSION['error'] = array();
				}
				$_SESSION['error'][] = 'Equipment with code - ' . $code . ' unavailable';
			}
		}
	}

	if ($added > 0) {
		$equipments = ($added == 1) ? 'Equipment' : 'Equipments';
		$_SESSION['success'] = $added . ' ' . $equipments . ' successfully borrowed';
	}
} else {
	$_SESSION['error'] = 'Fill up add form first';
}

header('location: transaction.php');
